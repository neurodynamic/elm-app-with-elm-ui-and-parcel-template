A basic Elm app template setup to use [elm-ui](https://github.com/mdgriffith/elm-ui) for views, [Parcel.js](https://parceljs.org) for building the app, and [elm-test](https://github.com/elm-community/elm-test) for testing.

# To use:

## Clone this repo:

`git clone git@gitlab.com:neurodynamic/elm-app-with-elm-ui-and-parcel-template.git project_name`

## Install packages:

`npm install`

## Run the app:

`npm start`

# Thanks!

Thanks to the authors of [create-elm-app](https://github.com/halfzebra/create-elm-app), from which I borrowed the elm-test example tests, and [25-elm-examples](https://github.com/bryanjenningz/25-elm-examples) from which I took inspiration and some sample code for the counter app.