module Main exposing (main)

import Browser
import Html exposing (Html)
import Main.Actions exposing (Action)
import Main.Model as Model exposing (Model)
import Main.Update exposing (update)
import Main.View exposing (view)


main : Program String Model Action
main =
    Browser.document
        { init = \flags -> ( Model.init flags, Cmd.none )
        , view =
            \model ->
                { title = model.pageTitle
                , body = [ view model ]
                }
        , update = update
        , subscriptions = \_ -> Sub.none
        }
