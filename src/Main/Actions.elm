module Main.Actions exposing (Action(..))


type Action
    = Increment
    | Decrement
