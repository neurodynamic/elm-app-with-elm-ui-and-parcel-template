module Main.Model exposing (Model, init)


type alias Model =
    { pageTitle : String
    , count : Int
    }


init : String -> Model
init pageTitle =
    { pageTitle = pageTitle
    , count = 0
    }
