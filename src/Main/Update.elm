module Main.Update exposing (update)

import Main.Actions exposing (Action(..))
import Main.Model exposing (Model)


update : Action -> Model -> ( Model, Cmd Action )
update action model =
    case action of
        Increment ->
            ( { model | count = model.count + 1 }, Cmd.none )
        Decrement ->
            ( { model | count = model.count - 1 }, Cmd.none )
