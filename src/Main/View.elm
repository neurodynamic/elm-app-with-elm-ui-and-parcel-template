module Main.View exposing (view)

import Element exposing (..)
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html)
import Main.Actions exposing (Action(..))
import Main.Model exposing (Model)


view : Model -> Html Action
view model =
    layout [ width fill, height fill ]
        (column
            [ centerX
            , centerY
            , Font.center
            , spacing 10
            ]
            [ counterButton Increment "+"
            , paragraph [ Font.size 40 ] [ text (String.fromInt model.count) ]
            , counterButton Decrement "-"
            ]
        )


counterButton : Action -> String -> Element Action
counterButton action labelString =
    Input.button
        [ centerX
        , centerY
        , width (px 80)
        , height (px 80)
        , Font.size 50
        , Border.width 2
        , Border.color (rgb 0 0 0)
        , Border.rounded 5
        , paddingXY 20 10
        ]
        { onPress = Just action
        , label = text labelString
        }
